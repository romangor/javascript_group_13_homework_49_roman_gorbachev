const createChessBoard = (size: number) => {
    let board = '';
    for(let i = 1; i < size * size; i++) {
        if((i % (size + 1)) === 0){
            board += '\n';
        } else if (i % 2 !== 0){
            board += '  ';
        } else {
            board += "██";
        }
    }
    return board;
};

console.log(createChessBoard(8));